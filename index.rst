.. |label| replace:: Sprachwechsel-Erweiterung
.. |snippet| replace:: FvSubshopSelect
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.4.0
.. |maxVersion| replace:: 5.5.3
.. |version| replace:: 1.2.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Mit diesem Plugin können Sie bei Subshops und Sprachshops die Auswahl der Shops mit Text und/ohne Flaggen zur Auswahl geben um in den entsprechenden Shops zu springen.
Des Weiteren können die Kategorienseiten und Shopseiten wenn eine ID entsprechend gepflegt ist, ebenso in den Sprachen wechseln und nicht wie es im Standard ist auf die Startseite weitergeleitet zu werden.
Sie können pro Subshop jeden Shop eine Flagge hinterlegen was im frontend mit angezeigt werden kann.

Frontend
--------
Im Header wird wenn Sie das die Funktion aktiviert werden, der Subshop-Wechsel angezeigt. Sie haben mehrere Möglichkeiten um dies darzustellen, je nachdem was Sie im Backend konfigurieren.

Anzeige mit Flaggen Horizontal

.. image:: FvSubshopSelect2.png

Anzeige mit Flaggen Vertikal

.. image:: FvSubshopSelect3.png

Anzeige ohne Flaggen Horizontal

.. image:: FvSubshopSelect4.png

Anzeige ohne Flaggen Vertikal

.. image:: FvSubshopSelect5.png

Backend
-------


Hier können Sie das Plugin konfigurieren pro Subshop.

.. image:: FvSubshopSelect1.png

:Aktiviere eigene Sprachauswahl: Ja bedeutet es wird aktiviert, mit Nein ist es deaktiviert.
:Aktiviere automatische Kategorie-Sprachwechsel: Die Aktivierung funktioniert nur in Zusammenspiel mit dem 1stVision Connector für Shopware.
:Aktiviere automatische Shopseiten-Sprachwechsel: Die Aktivierung funktioniert nur wenn bei den Shopseiten die SprachID bei den entsprechenden Shopseiten gepflegt ist.

.. image:: FvSubshopSelect6.png

:Anzeige der Sprachauswahl: Hier können Sie Auswählen ob die Anzeige Horizontal oder vertikal angezeigt werden soll.
:Shop <Name des Subshops/Sprachshops> auswählbar: Hier definieren Sie ob in dem Subshop(Reiter) dieser Subshop auswählbar ist. Es werden die Subshops hier dynamisch erweitert.
:Flagge <Name des Subshops/Sprachshops>: Hier können Sie das Bild dem Shop zuordnen.



technische Beschreibung
------------------------

Beim installieren des Plugins wird automatisch ein Album in der Medien-Verwaltung Namens "Flaggen" angelegt und entsprechend die Thumbnails dazu sind bereits vordefiniert.


Modifizierte Template-Dateien
-----------------------------
:/widgets/index/shop_menu.tpl:




